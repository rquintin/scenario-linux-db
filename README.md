# Linux/Java Scenario

This scenario will assess your ability to manage Linux and Java.

## Setup

## Create a Docker ID account
In order to execute this scenario, you will need a Docker ID.
Go [here](https://docs.docker.com/docker-id/) for instructions to create a _free_ Docker ID.
Follow the instructions to setup your Docker ID.

Go to the [Docker Playground](http://play-with-docker.com) and confirm that you are not a robot. 
In the left-hand menu, click on "ADD NEW INSTANCE"  
This will start a Docker instance (node) and log you in as the root user. Feel free to play around.  
When you are ready, run the command below to startup a server..

`curl -o runme.sh https://code.vt.edu/rquintin/scenario-linux-db/raw/master/runme.sh && bash ./runme.sh`

After some time setting up the environment, 
you should see a bunch of output that ends in something like this:

```
 ---> 9381c51f929c
Removing intermediate container 684efb79b19b
Step 7/8 : WORKDIR /
 ---> a98b5c59f867
Removing intermediate container 22e54d73cdee
Step 8/8 : CMD bash
 ---> Running in a129cb94058e
 ---> bdc8fd8e8666
Removing intermediate container a129cb94058e
Successfully built bdc8fd8e8666
+ docker run --rm -it scenario
```

## Scenario

**Q1.** What user are you logged in as? What command would you use to check?

**Q2.** What command would you use to find all _files_ in directory `/find-files` that have been modified in the last week?

**Q3.** Execute the command from the previous question and provide the output.

**Q4.** In directory `/data` there are several comma separated files. The fourth column of each of these files
contains either "Online" or "Offline". How would you update each of these files to replace the word "Online"
in the fourth column with the word "Internet"?

**Q5.** There is a Postgresql database running. What command would you use to connect to the database?

**Q6.** The data in `/data` has been loaded into a table called `orders` in the Postgresql database. How would you query this table to find the sum of total profit by each region?

**Q7.** Given the query `select count(*) from orders where region = 'Asia';`, what might you do to ensure the best performance for this query?

**Q8.** How would you test your answer to the previous question to ensure that your solution actually does improve performance?

**Q9.** What command(s) would you execute to create a new table called `asia_orders` that contained a copy of all of the rows from `orders` where `region = 'Asia'`?
