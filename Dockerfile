FROM postgres

# Install
ENV DEBIAN_FRONTEND=noninteractive
ENV TERM=xterm

RUN apt-get update && \
    apt-get install -y \
      vim \
    && \
    rm -r /var/lib/apt/lists/* 
ADD bin/* /usr/local/bin/
ADD data /data
ADD sql /sql

RUN chmod a+rx /usr/local/bin/* && \
    sleep 1 && \
    /usr/local/bin/setup.sh && \
    rm /usr/local/bin/setup.sh

WORKDIR /

CMD ["start.sh"]
