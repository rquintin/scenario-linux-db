#!/bin/bash
set -ex
git clone https://code.vt.edu/rquintin/scenario-linux-db.git
cd scenario-linux-db
docker build -t scenario . && docker run --rm -it scenario
