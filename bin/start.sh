#!/bin/bash

nohup /usr/local/bin/docker-entrypoint.sh postgres >/var/log/postgres.log 2>&1 &

echo "Starting up postgres.."
sleep 1
while [ -z "$(grep 'database system is ready to accept connections' /var/log/postgres.log)" ]; do
  sleep 1
done

psql -U postgres < /sql/cr_table.sql >/dev/null 2>&1
rm /sql/*
clear
exec bash
