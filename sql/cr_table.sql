create table orders
(
  region varchar(50),
  country varchar(50),
  item_type varchar(25),
  sales_channel varchar(25),
  order_priority varchar(25),
  order_date date,
  order_id varchar(25),
  ship_date date,
  units_sold int,
  unit_price numeric(10,2),
  unit_cost numeric(10,2),
  total_revenue numeric(10,2),
  total_cost numeric(10,2),
  total_profit numeric(10,2)
);

copy orders from '/data/a.csv' delimiter ',' csv header;
copy orders from '/data/b.csv' delimiter ',' csv header;
copy orders from '/data/c.csv' delimiter ',' csv header;
copy orders from '/data/d.csv' delimiter ',' csv header;
copy orders from '/data/e.csv' delimiter ',' csv header;
copy orders from '/data/f.csv' delimiter ',' csv header;
copy orders from '/data/g.csv' delimiter ',' csv header;
copy orders from '/data/h.csv' delimiter ',' csv header;
copy orders from '/data/i.csv' delimiter ',' csv header;
copy orders from '/data/j.csv' delimiter ',' csv header;
copy orders from '/data/k.csv' delimiter ',' csv header;
copy orders from '/data/l.csv' delimiter ',' csv header;
copy orders from '/data/m.csv' delimiter ',' csv header;
copy orders from '/data/n.csv' delimiter ',' csv header;
copy orders from '/data/o.csv' delimiter ',' csv header;
copy orders from '/data/p.csv' delimiter ',' csv header;
copy orders from '/data/q.csv' delimiter ',' csv header;
copy orders from '/data/r.csv' delimiter ',' csv header;
copy orders from '/data/s.csv' delimiter ',' csv header;
copy orders from '/data/t.csv' delimiter ',' csv header;
copy orders from '/data/u.csv' delimiter ',' csv header;
copy orders from '/data/v.csv' delimiter ',' csv header;
copy orders from '/data/w.csv' delimiter ',' csv header;
copy orders from '/data/x.csv' delimiter ',' csv header;
copy orders from '/data/y.csv' delimiter ',' csv header;
copy orders from '/data/z.csv' delimiter ',' csv header;

insert into orders select * from orders;
insert into orders select * from orders;
insert into orders select * from orders;
insert into orders select * from orders;

